const express = require('express');
const router = express.Router();

const UsersControllers =  require("../controllers/user");
const checkAuth = require('../middleware/check-auth');

router.post('/signup', UsersControllers.users_sugnUp );
router.post('/login', UsersControllers.users_login);
router.delete('/:userId', checkAuth, UsersControllers.users_delete);

module.exports = router;